//
//  ViewController.swift
//  mitm_target_ios
//
//  Created by Oleksii Horishnii on 02.05.2020.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!
    
    let urls = [
        "http://httpbin.org/user-agent",
        "https://httpbin.org/user-agent",
        "https://postman-echo.com/get?foo1=bar1&foo2=bar2"
    ]
    @IBAction func buttonClicked(_ sender: Any) {
        DispatchQueue.main.async {
            self.label.text = "loading..."
        }
        
        var results: [Int: String] = [:]
        func checkIfDone() {
            if results.count == urls.count {
                var str = ""
                for idx in 0..<urls.count {
                    str += results[idx] ?? ""
                }
                DispatchQueue.main.async {
                    self.label.text = str
                }
            }
        }
        
        for (idx, url) in self.urls.enumerated() {
            self.requestWith(url: url) { response in
                results[idx] = response
                checkIfDone()
            }
        }
    }
    
    func requestWith(url: String,
                     callback: @escaping ((String) -> Void)) {
        let url = URL(string: url)!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let error = error {
                callback(String(describing: error) + "\n\n")
                return
            }
            var body: String = ""
            if let data = data,
                let str = String(data: data, encoding: .utf8) {
                body = str
            }
            let out = """
            url: \(url)
            code: \((response as? HTTPURLResponse)?.statusCode ?? -1)
            body: \(body)
            """
            
            callback(out + "\n\n")
        }
        
        task.resume()
    }
}

